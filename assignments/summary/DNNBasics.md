**AI:DNN Basics**

*Neaural Networks is basically inspired by brain to aid machines with intelligence.*
>sturcture of a neural network ![neural_network](uploads/3e1883aee7b96246c7082a56c1474cfd/neural_network.png)

**Deep Neural Networks** is a layered sturcture of neurons with a feedback mechanism as follows:
There are three layers in it
- input layer
- hidden layer
- output layer


> Structue of DNN ![DNN_Structure](uploads/45068e6c268704196e6088089291a592/DNN_Structure.png)
blue cirlces represents a **Neuron**

- Each Neuron has it's respective Activation(a) number.
- Weights(w) to be assigned to ech neuron of first layer.
All the activations of the first layer are taken and then compute to the weighted sum of all.

> Activation Function ![Activation_Function](uploads/569cdaae802fa51d03e8504c25afaed7/Activation_Function.png)

Sigmoid Function is one of the simple activation function used.

> ![sigmoid](uploads/7638cf033c38b251326e0f369239fa14/sigmoid.png)

The funcion squishes the range of the respective function within 0 & 1.
But, before we introduce the simoid function , we add **Bias(b)**, so that whenever we want say a weighted sum o/p nt only those >10 or >20 then we add or subtarct tht paricular digit as following:

Matrix Multiplication is taken in account for accurate calculation:![sigmoid_calculation](uploads/d3038bede0a015a1da22f12e4b32b9ad/sigmoid_calculation.jpg)


Thus,
w- *signifies what pixel pattern this neuron in the second layer is picking up.*
b- *tells how high or low the weighted sum needds to be before neuron starts gettng meaningfully active.*

Instead of Sigmoid we use ***ReLU (Rectified Linear Unit)***
![ReLU_function](uploads/3644421de28ffa652bbd278cd46f930c/ReLU_function.png)

Function: taking maximum of 0 & a(activation no. given)

**GRADIENT DESCENT**

Gradient Descent of a function gives the direction of steepest ascent.*(Basically, which direction should you step to increase the function most quickly)*


**BACKPROPOGATION**
![BACKPROPOGATION](uploads/bde41254f7535d4cd5701619556e14ba/BACKPROPOGATION.png)

The Algorithm for determining how a single tarining example would like to nudge, the weights and biases in terms of what relative propoortions to those changes causes the most rapid decrease to the cost.
Backpropogation is basically for calculating Gradients efficiently.
note:* The negative of gradient gives the directions to step that decrease the function most quickly.*

- Start from the output layer and propagate backwards, updating weights and biases for each layer
- Randomly wesudivide the data available into mini batches & compute each step with respect to  mini batch.

RESULT : We converge towards a local minimum of cost.
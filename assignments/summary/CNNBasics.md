**CNN : Convolution Neual Network**

CNN is a class od Deep Neural Network(DNN), also known as ConvNet basically used for classification.

*Areas used*: images recognition, images classifications, Objects detections, recognition faces etc.

1. Input : image into convolution layer

2. Choose parameters, apply filters(matrix) with strides, padding if requires. Perform convolution on the image and apply ReLU activation to the matrix.

- ***Strides***: Stride is the number of pixels shifts over the input matrix.
- ***Kernels***: Concatenation of filters.
- ***Padding***: If filter doesn't fit properly, padding takes place which are of two types.


       - Zero Padding:pad with enough zeros till it fits.
       - Valid Padding:keeping only the valid part, & drop the remaining where filter doesn't fit.

- ***ReLU***: Introduces non-linearity(to learn non-negative liner values).

> ![Relu](uploads/1a612ba28a0c0995e9a64eadf59040cf/Relu.png)

3. Perform pooling to reduce dimensionality, reduces the number of parameters when the images are too large.

Spatial pooling reduces the dimensionality of each map but retains important information, having further types:

       -Max Pooling
       -Average Pooling
       -Sum Pooling

> Pooling: ![pooling](uploads/68021b60c8c4302bce980011789c4856/pooling.jpg)

(*Max pooling takes the largest element from the rectified feature map. Taking the largest element could also take the average pooling. Sum of all elements in the feature map call as sum pooling.*)

4. Add as many convolutional layers until satisfied.

5. Flatten the output and feed into a fully connected layer (FC Layer).Flattening layer is used to bring all feature maps matrices into a single vector.

6. Output the class using an activation function (Logistic Regression with cost functions) and classifies images.
*Softmax is used at the output layer for classification.*

> Final Process visually  looks like this: ![CNN](uploads/571366cf6eb33655407409af6e55c7c7/CNN.jpeg) 



